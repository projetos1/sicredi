## Case Sicredi

**Informações sobre o projeto**

1. Linguagens Open JDK 11, utilizando as ferramentas do spring boot 2.5.4 e MySQL para gerenciamentos dos dados.
2. Para rodar o projeto precisa do lombok instalado e o MySQL para conexão ao banco de dados e criação da estrutura.
3. Para realizar as chamadas ao sistema utilize o Postman o arquivo para import já consta dentro da branch (Sicredi.postman_collection.json).

---

## Versão 1.0.0