package com.br.sicredi.model.enums;

public enum EnumVoto {

	NAO(0),
	SIM(1);
	
	private Integer value;

	public static EnumVoto get(Integer value) {
		if (value == null) {
			return null;
		}

		for (EnumVoto tipo : EnumVoto.values()) {
			if (tipo.getValue().equals(value)) {
				return tipo;
			}
		}
		return null;
	}
	
	EnumVoto(Integer value) {
        this.value = value;
    }

//	@JsonValue
    public Integer getValue() {
        return value;
    }
	
}
