package com.br.sicredi.service;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.br.sicredi.exception.EnumErrorException;
import com.br.sicredi.exception.ServiceException;
import com.br.sicredi.model.Associado;
import com.br.sicredi.model.Empresa;
import com.br.sicredi.model.dto.AssociadoDTO;
import com.br.sicredi.repository.AssociadoRepository;

@Service
public class AssociadoService {

	@Autowired
	private AssociadoRepository repository;
	
	@Autowired
	private EmpresaService empresaService;
	
	private static String urlCpfApi = "https://user-info.herokuapp.com/users/";
	
	public List<Associado> listarTodos() {
		return repository.findAll();
	}

	public Associado buscarPorId(Long associadoId) {
		return repository.findById(associadoId).orElse(null);
	}

	public Associado criar(AssociadoDTO dto) {
		if(!validarCPF(dto.getCpf().replaceAll("[^0-9]", ""))) {
			throw new ServiceException(EnumErrorException.PARAMETROS_INVALIDOS, "CPF é inválido!");
		}
		
		Associado associado = new Associado();
		BeanUtils.copyProperties(dto, associado, "empresaId");
		Empresa empresa = empresaService.buscarPorId(dto.getEmpresaId());
		if(empresa == null) {
			throw new ServiceException(EnumErrorException.PARAMETROS_INVALIDOS, "Empresa não está cadastrada!");
		}
		associado.setEmpresa(empresa);
		
		return repository.save(associado);
	}

	private boolean validarCPF(String cpf) {
		try {
			RestTemplate rest  = new RestTemplate();
			ResponseEntity<?> result = rest.getForEntity(urlCpfApi + cpf, Object.class);
			HttpStatus statusCode = result.getStatusCode();
			if(statusCode.equals(HttpStatus.OK)) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			throw new ServiceException(EnumErrorException.ERROR_VALIDA_CPF_API);
		}
	}

}
