package com.br.sicredi.model.dto;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.br.sicredi.model.enums.EnumTipoTempoVotacao;

import lombok.Data;

@Data
public class AssembleiaDTO {

	@NotBlank(message = "Nome da pauta é obrigatório!")
	private String pauta;
	
	@NotNull(message = "Código da empresa é obrigatório!")
	private Long empresaId;
	
	private Integer tempoVotacao;
	
	@Enumerated(EnumType.ORDINAL)
	private EnumTipoTempoVotacao tipoTempoVotacao;
	
}
