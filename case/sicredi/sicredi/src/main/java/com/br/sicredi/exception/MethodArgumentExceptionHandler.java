package com.br.sicredi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.br.sicredi.util.ValidMessages;

@ControllerAdvice
public class MethodArgumentExceptionHandler {

	@ExceptionHandler(value = MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ResponseEntity<Object> handler(MethodArgumentNotValidException exception) {

		BindingResult result = exception.getBindingResult();

		return ResponseEntity.badRequest().body(ValidMessages.retornaMensagemErro(result));

	}

}
