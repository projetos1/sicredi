package com.br.sicredi.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.br.sicredi.model.Assembleia;
import com.br.sicredi.model.dto.AssembleiaDTO;
import com.br.sicredi.service.AssembleiaService;

@RestController
@RequestMapping("/assembleias")
public class AssembleiaController {

	@Autowired
	private AssembleiaService service;
	
	@GetMapping
	public List<Assembleia> listarTodas() {
		return service.listarTodas();
	}
	
	@PostMapping("/criarPauta")
	public Assembleia criarPauta(@Valid @RequestBody AssembleiaDTO dto) {
		return service.criarPauta(dto);
		
	}
	
}
