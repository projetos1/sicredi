package com.br.sicredi.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.br.sicredi.model.Associado;
import com.br.sicredi.model.dto.AssociadoDTO;
import com.br.sicredi.service.AssociadoService;

@RestController
@RequestMapping("/associados")
public class AssociadoController {

	@Autowired
	private AssociadoService service;
	
	@GetMapping
	public List<Associado> listarTodos() {
		return service.listarTodos();
	}
	
	@ResponseStatus(code = HttpStatus.CREATED)
	@PostMapping("/criar")
	public Associado criar(@Valid @RequestBody AssociadoDTO dto) { 
		return service.criar(dto);
	}
	
}
