package com.br.sicredi.controller;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.br.sicredi.model.dto.VotoDTO;
import com.br.sicredi.service.VotacaoService;

@RestController
@RequestMapping("/votacoes")
public class VotacaoController {

	@Autowired
	private VotacaoService service;
	
	@ResponseStatus(code = HttpStatus.CREATED)
	@PostMapping("/voto")
	public ResponseEntity<?> voto(@Valid @RequestBody VotoDTO dto) {
		service.voto(dto);
		return ResponseEntity.ok().build();
	}
	
	@ResponseStatus(code = HttpStatus.OK)
	@GetMapping("/assembleia")
	public ResponseEntity<?> resultadoDaVotacao(@Valid 
			@NotNull(message = "O código da assembleia é obrigatório!")
			@RequestParam("assembleiaId") Long assembleiaId) {
		return ResponseEntity.ok(service.resultadoDaVotacao(assembleiaId));
	}
	
}
