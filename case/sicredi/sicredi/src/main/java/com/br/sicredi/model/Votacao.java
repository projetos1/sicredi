package com.br.sicredi.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.br.sicredi.model.enums.EnumVoto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Votacao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull(message = "Voto é obrigatório!")
	@Enumerated(EnumType.ORDINAL)
	private EnumVoto voto;
	
	@ManyToOne
	@JoinColumn(name="associado_id", nullable = false)
	private Associado associado;
	
	@ManyToOne
	@JoinColumn(name="assembleia_id", nullable = false)
	private Assembleia assembleia;
	
}
