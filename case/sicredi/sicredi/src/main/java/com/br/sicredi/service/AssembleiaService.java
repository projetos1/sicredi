package com.br.sicredi.service;

import java.time.LocalDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.sicredi.model.Assembleia;
import com.br.sicredi.model.Empresa;
import com.br.sicredi.model.dto.AssembleiaDTO;
import com.br.sicredi.model.enums.EnumTipoTempoVotacao;
import com.br.sicredi.repository.AssembleiaRepository;

@Service
public class AssembleiaService {

	@Autowired
	private AssembleiaRepository repository;
	
	@Autowired
	private EmpresaService empresaService;
	
	public List<Assembleia> listarTodas() {
		return repository.findAll();
	}

	@Transactional
	public Assembleia criarPauta(AssembleiaDTO dto) {
		Assembleia assembleia = new Assembleia();
		BeanUtils.copyProperties(dto, assembleia, "empresaId");

		Empresa empresa = empresaService.buscarPorId(dto.getEmpresaId());
		assembleia.setEmpresa(empresa);
		assembleia.setDataAbertura(LocalDateTime.now());
		
		if(assembleia.getTempoVotacao() != null) {
			adicionandoPeriodoVotacao(assembleia);
		} else {
			assembleia.setDataFechamento(LocalDateTime.now().plusMinutes(1));
		}
		
		System.out.println("Assembleia " + assembleia);
		return repository.save(assembleia);
	}

	private void adicionandoPeriodoVotacao(Assembleia assembleia) {

		if(EnumTipoTempoVotacao.HORA.equals(assembleia.getTipoTempoVotacao())) {
			assembleia.setDataFechamento(LocalDateTime.now().plusHours(assembleia.getTempoVotacao()));
		} else if(EnumTipoTempoVotacao.MINUTO.equals(assembleia.getTipoTempoVotacao())) {
			assembleia.setDataFechamento(LocalDateTime.now().plusMinutes(assembleia.getTempoVotacao()));
		} else if(EnumTipoTempoVotacao.SEGUNDO.equals(assembleia.getTipoTempoVotacao())) {
			assembleia.setDataFechamento(LocalDateTime.now().plusSeconds(assembleia.getTempoVotacao()));
		}
	}

	public Assembleia buscarPorId(Long assembleiaId) {
		return repository.findById(assembleiaId).orElse(null);
	}


}
