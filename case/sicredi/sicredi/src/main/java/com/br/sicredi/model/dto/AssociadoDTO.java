package com.br.sicredi.model.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class AssociadoDTO {

	@NotBlank(message = "Nome é obrigatório!")
	private String nome;
	
//	@CPF(message = "CPF inválido!")
	@NotBlank(message = "CPF é obrigatório!")
	private String cpf;
	
	@NotNull(message = "Código da Empresa é obrigatório!")
	private Long empresaId;
	
	
}
