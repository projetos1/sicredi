package com.br.sicredi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.br.sicredi.model.Assembleia;

@Repository
public interface AssembleiaRepository extends JpaRepository<Assembleia, Long> {

}
