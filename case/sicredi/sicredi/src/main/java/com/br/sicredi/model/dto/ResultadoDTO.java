package com.br.sicredi.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResultadoDTO {

	private String pauta;
	private Integer sim;
	private Integer nao;
	
	
}
