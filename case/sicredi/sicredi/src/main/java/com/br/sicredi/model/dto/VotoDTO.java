package com.br.sicredi.model.dto;

import javax.validation.constraints.NotNull;

import com.br.sicredi.model.enums.EnumVoto;

import lombok.Data;

@Data
public class VotoDTO {

	@NotNull(message = "Código do associado é obrigatório!")
	private Long associadoId;
	
	@NotNull(message = "Código da assembleia é obrigatória!")
	private Long assembleiaId;
	
	@NotNull(message = "Voto é obrigatório!")
	private EnumVoto voto;
	
}
