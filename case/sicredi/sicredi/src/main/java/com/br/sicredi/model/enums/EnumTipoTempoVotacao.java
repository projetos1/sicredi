package com.br.sicredi.model.enums;

public enum EnumTipoTempoVotacao {

	HORA(0),
	MINUTO(1),
	SEGUNDO(2);
	
	private Integer value;

	public static EnumTipoTempoVotacao get(Integer value) {
		if (value == null) {
			return null;
		}

		for (EnumTipoTempoVotacao tipo : EnumTipoTempoVotacao.values()) {
			if (tipo.getValue().equals(value)) {
				return tipo;
			}
		}
		return null;
	}
	
	EnumTipoTempoVotacao(Integer value) {
        this.value = value;
    }

//	@JsonValue
    public Integer getValue() {
        return value;
    }
	
}
