package com.br.sicredi.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;

import org.springframework.data.annotation.CreatedDate;

import com.br.sicredi.model.enums.EnumTipoTempoVotacao;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Assembleia {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotBlank(message = "A Pauta é obrigatória!")
	private String pauta;
	
	@CreatedDate
    @Column(name = "data_abertura", nullable = false, updatable = false)
	private LocalDateTime dataAbertura;
	
    @Column(name = "data_fechamento", nullable = false, updatable = false)
	private LocalDateTime dataFechamento;
	
	@Column(name = "tempo_votacao")
	private Integer tempoVotacao;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "tipo_tempo_votacao")
	private EnumTipoTempoVotacao tipoTempoVotacao;
	
	@ManyToOne
	@JoinColumn(name="empresa_id", nullable = false)
	private Empresa empresa;
}
