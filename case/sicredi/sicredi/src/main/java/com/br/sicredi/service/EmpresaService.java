package com.br.sicredi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.sicredi.model.Empresa;
import com.br.sicredi.repository.EmpresaRepository;

@Service
public class EmpresaService {

	@Autowired
	private EmpresaRepository repository;
	
	public List<Empresa> listarTodas() {
		return repository.findAll();
	}

	public Empresa buscarPorId(Long empresaId) {
		return repository.findById(empresaId).orElse(null);
	}

}
