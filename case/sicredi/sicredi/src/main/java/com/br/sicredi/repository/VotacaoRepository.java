package com.br.sicredi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.br.sicredi.model.Votacao;

@Repository
public interface VotacaoRepository extends JpaRepository<Votacao, Long> {

	public Votacao findByAssembleiaIdAndAssociadoId(Long assembleiaId, Long associadoId);

	public List<Votacao> findByAssembleiaId(Long assembleiaId);

}
