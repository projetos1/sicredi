package com.br.sicredi.service;

import java.time.LocalDateTime;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.sicredi.exception.EnumErrorException;
import com.br.sicredi.exception.ServiceException;
import com.br.sicredi.model.Assembleia;
import com.br.sicredi.model.Votacao;
import com.br.sicredi.model.dto.ResultadoDTO;
import com.br.sicredi.model.dto.VotoDTO;
import com.br.sicredi.model.enums.EnumVoto;
import com.br.sicredi.repository.VotacaoRepository;

@Service
public class VotacaoService {
	
	@Autowired
	private VotacaoRepository repository;
	
	@Autowired
	private AssociadoService associadoService;
	
	@Autowired
	private AssembleiaService assembleiaService;

	public void voto(@Valid VotoDTO dto) {
		
		if(!validarVoto(dto)) {
			throw new ServiceException(EnumErrorException.VOTO_JA_REALIZADO);
		}
		
		Assembleia assembleia = assembleiaService.buscarPorId(dto.getAssembleiaId());
		if(assembleia.getDataFechamento().isBefore(LocalDateTime.now())) {
			throw new ServiceException(EnumErrorException.ASSEMBLEIA_FINALIZADA);
		}
		
		Votacao votacao = new Votacao();
		BeanUtils.copyProperties(dto, votacao, "associado_id", "assembleia_id");
		
		votacao.setAssociado(associadoService.buscarPorId(dto.getAssociadoId()));
		votacao.setAssembleia(assembleia);
		
		try {
			repository.save(votacao);
		} catch (ServiceException e) {
			throw new ServiceException(EnumErrorException.ERROR_SAVE);
		}
	}

	private boolean validarVoto(@Valid VotoDTO dto) {
		Votacao  votacao = repository.findByAssembleiaIdAndAssociadoId(dto.getAssembleiaId(), dto.getAssociadoId());
		
		if(votacao != null) {
			return false;
		}
		
		return true;
	}

	public ResultadoDTO resultadoDaVotacao(Long assembleiaId) {
		
		List<Votacao> listVotos = repository.findByAssembleiaId(assembleiaId);
		String pauta = listVotos.get(0).getAssembleia().getPauta();
		Integer qtdeSim = 0;
		Integer qtdeNao = 0;
		
		for(Votacao voto : listVotos) {
			if(voto.getVoto().equals(EnumVoto.SIM)) qtdeSim++; else qtdeNao++;
		}
		
		ResultadoDTO resultado = new ResultadoDTO(pauta, qtdeSim, qtdeNao);
		return resultado;
	}

}
