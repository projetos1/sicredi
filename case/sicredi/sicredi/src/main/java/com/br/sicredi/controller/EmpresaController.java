package com.br.sicredi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.br.sicredi.model.Empresa;
import com.br.sicredi.service.EmpresaService;

@RestController
@RequestMapping("/empresas")
public class EmpresaController {

	@Autowired
	private EmpresaService service;
	
	@GetMapping
	public List<Empresa> listarTodas() {
		return service.listarTodas();
	}

}
