-- Insert Empresas
insert into empresa(id, nome, cnpj) values (1, 'Coca Cola', '84.874.259/0001-51');
insert into empresa(id, nome, cnpj) values (2, 'Apple', '22.883.117/0001-01');

-- Insert Associados
insert into associado(id, nome, cpf, empresa_id) values (1, 'Abílio', '826.286.530-87', 1);
insert into associado(id, nome, cpf, empresa_id) values (2, 'José', '437.798.970-75', 1);
insert into associado(id, nome, cpf, empresa_id) values (3, 'Rafaela', '115.401.120-80', 1);

insert into associado(id, nome, cpf, empresa_id) values (4, 'Matheus', '911.190.570-00', 2);
insert into associado(id, nome, cpf, empresa_id) values (5, 'Maria', '666.561.930-30', 2);
insert into associado(id, nome, cpf, empresa_id) values (6, 'Gustavo', '293.321.930-10', 2);

