-- Criação da tabela de Empresa
create table if not exists empresa (
	`id` bigint(11) UNSIGNED not null auto_increment, 
	`nome` varchar(100) not null, 
	`cnpj` varchar(18) not null,
	
	primary key(`id`)
) ENGINE=InnoDB;

-- Criação da tabela de Associado
create table if not exists associado (
	`id` bigint(11) UNSIGNED not null auto_increment,
    `nome` varchar(80) not null,
    `cpf` varchar(14) not null,
	`empresa_id` bigint(11) UNSIGNED not null,
	
	primary key(`id`),
    CONSTRAINT `fk_associado_on_empresa_id`
    FOREIGN KEY (`empresa_id`)
    REFERENCES `empresa` (`id`)
) ENGINE=InnoDB;

-- Criaçao da tabela de Assembleia
create table if not exists assembleia (
	`id` bigint(11) UNSIGNED not null auto_increment,
    `pauta` varchar(80) not null,
    `data_abertura` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `data_fechamento` TIMESTAMP NOT NULL,
    `tempo_votacao` INT(11) NULL,
    `tipo_tempo_votacao` TINYINT(1) NOT NULL COMMENT '0 = Hora\n1 = Minuto\n2 = Segundo',
    `empresa_id` bigint(11) UNSIGNED NOT NULL,
    
    primary key(`id`),
    CONSTRAINT `fk_assembleia_on_empresa_id`
    FOREIGN KEY (`empresa_id`)
    REFERENCES `empresa` (`id`)
) ENGINE=InnoDB;

-- Criação da tabela de Votação
create table if not exists votacao (
	`id` bigint(11) UNSIGNED not null auto_increment,
	`voto` boolean not null,
	`associado_id` bigint(11) UNSIGNED not null,
	`assembleia_id` bigint(11) UNSIGNED not null,
	
	primary key(`id`),
	CONSTRAINT `fk_votacao_on_associado_id`
    FOREIGN KEY (`associado_id`) REFERENCES `associado` (`id`),
    CONSTRAINT `fk_votacao_on_assembleia_id`
    FOREIGN KEY (`assembleia_id`) REFERENCES `assembleia` (`id`)
) ENGINE=InnoDB;